package com.debateapp.debateapp;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;


@SpringBootApplication
public class DebateAppApplication {

    public static void main(String[] args)
    {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(DebateAppApplication.class);
        builder.headless(false);
        builder.run(args);
    }

}
