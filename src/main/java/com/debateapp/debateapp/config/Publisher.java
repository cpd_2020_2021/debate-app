package com.debateapp.debateapp.config;

import com.debateapp.debateapp.dto.Message;
import com.debateapp.debateapp.dto.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Publisher {

    private RabbitTemplate template;
    Logger logger = LoggerFactory.getLogger(Publisher.class);

    public static boolean publisherEducation = false;
    public static boolean publisherFinance = false;
    public static boolean publisherArt = false;

    @Autowired
    public Publisher(RabbitTemplate template){
        this.template = template;
    }

    public void publish(Topic topic, Message message){
        if (null != message){
            switch (topic){
                case Education:
                    if (!publisherEducation) return;
                    template.convertAndSend(MessagingConfig.EDUCATION_EXCHANGE, "", message);
                    break;
                case Finance:
                    if (!publisherFinance) return;
                    template.convertAndSend(MessagingConfig.FINANCE_EXCHANGE, "", message);
                    break;
                case Art:
                    if (!publisherArt) return;
                    template.convertAndSend(MessagingConfig.ART_EXCHANGE,"", message);
                    break;
                default:
                    logger.info("[X] Invalid topic: {}", topic);
            }
            logger.info("[+] Publishing on {}: {}", topic, message);
        }
    }

}
