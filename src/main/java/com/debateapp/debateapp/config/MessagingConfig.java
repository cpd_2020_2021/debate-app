package com.debateapp.debateapp.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;


@Configuration
public class MessagingConfig {

    @Value("${spring.rabbitmq.queue_education}")
    private String EDUCATION_QUEUE;
    @Value("${spring.rabbitmq.queue-finance}")
    private String FINANCE_QUEUE;
    @Value("${spring.rabbitmq.queue-art}")
    private String ART_QUEUE;

    public static final String EDUCATION_EXCHANGE = "education_debate";
    public static final String FINANCE_EXCHANGE = "finance_debate";
    public static final String ART_EXCHANGE = "art_debate";

    @Bean
    public Queue queueEducation() {
        return new Queue(EDUCATION_QUEUE, false);
    }

    @Bean
    public Queue queueFinance() {
        return new Queue(FINANCE_QUEUE, false);
    }

    @Bean
    public Queue queueArt() {
        return new Queue(ART_QUEUE, false);
    }

    @Bean
    public FanoutExchange  exchangeEducation() {
        return new FanoutExchange (EDUCATION_EXCHANGE);
    }

    @Bean
    public FanoutExchange  exchangeFinance() {
        return new FanoutExchange (FINANCE_EXCHANGE);
    }

    @Bean
    public FanoutExchange  exchangeArt() {
        return new FanoutExchange (ART_EXCHANGE);
    }

    @Bean
    public Binding bindingEducation() {
        return BindingBuilder.bind(queueEducation()).to(exchangeEducation());
    }

    @Bean
    public Binding bindingFinance() {
        return BindingBuilder.bind(queueFinance()).to(exchangeFinance());
    }

    @Bean
    public Binding bindingArt() {
        return BindingBuilder.bind(queueArt()).to(exchangeArt());
    }

    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}
