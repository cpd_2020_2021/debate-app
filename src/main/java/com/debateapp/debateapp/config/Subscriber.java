package com.debateapp.debateapp.config;

import com.debateapp.debateapp.controller.LoginController;
import com.debateapp.debateapp.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

@Component
public class Subscriber {

    Logger logger = LoggerFactory.getLogger(Subscriber.class);

    private Environment environment;
    private String ip;

    public static boolean subscriberEducation = false;
    public static boolean subscriberFinance = false;
    public static boolean subscriberArt = false;

    List<Message> educationMessages;
    List<Message> financeMessages;
    List<Message> artMessages;

    @Autowired
    public Subscriber(Environment environment) throws UnknownHostException {
        this.environment = environment;
        this.ip = InetAddress.getLocalHost().getHostAddress();

        educationMessages = new ArrayList<>();
        financeMessages = new ArrayList<>();
        artMessages = new ArrayList<>();
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue_education}")
    public void consumeMessageFromEducationQueue(Message message) {
        if (!subscriberEducation) return;

        logger.info("[-] Message received from education queue : {}", message);
        if (! LoginController.userLogged.toLowerCase().equals(message.getPublisher().toLowerCase()))
        {
            educationMessages.add(message);
            String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.debate");
            new RestTemplate().put(url, message, Object.class);
        }
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue-finance}")
    public void consumeMessageFromFinanceQueue(Message message) {
        if (!subscriberFinance) return;

        logger.info("[-] Message received from finance queue : {}", message);
        if (! LoginController.userLogged.toLowerCase().equals(message.getPublisher().toLowerCase())) {
            financeMessages.add(message);
            String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.debate");
            new RestTemplate().put(url, message, Object.class);
        }
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue-art}")
    public void consumeMessageFromArtQueue(Message message) {
        if (!subscriberArt) return;

        logger.info("[-] Message received from art queue : {}", message);
        if (! LoginController.userLogged.toLowerCase().equals(message.getPublisher().toLowerCase())) {
            artMessages.add(message);
            String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.debate");
            new RestTemplate().put(url, message, Object.class);
        }
    }

    public List<Message> getEducationMessages() {
        return educationMessages;
    }

    public List<Message> getFinanceMessages() {
        return financeMessages;
    }

    public List<Message> getArtMessages() {
        return artMessages;
    }
}
