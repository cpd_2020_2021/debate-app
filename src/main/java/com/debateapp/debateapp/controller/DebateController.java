package com.debateapp.debateapp.controller;

import com.debateapp.debateapp.DebateAppApplication;
import com.debateapp.debateapp.dto.Message;
import com.debateapp.debateapp.dto.Person;
import com.debateapp.debateapp.gui.DebateGui;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
@RequestMapping("${endpoint.debate}")
public class DebateController {

    private Environment environment;
    private String ip;

    private DebateGui debateGui;

    @Autowired
    public DebateController(Environment environment, DebateGui debateGui) throws UnknownHostException {
        this.environment = environment;
        this.ip = InetAddress.getLocalHost().getHostAddress();

        this.debateGui = debateGui;
    }

    @PostMapping
    public void sendMessage(@RequestBody Message message){
        String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.pubsub");
        new RestTemplate().postForObject(url, message, Object.class);
    }

    @PutMapping
    public void receiveMessage(@RequestBody Message message){
        debateGui.displayMessage(message);
    }

    @PostMapping("/opendebates")
    public void setVisibleDebateGui(@RequestBody Person person){
        this.debateGui.setVisibility(true, person);
    }

    @PostMapping("/tokenreceived")
    public void canSpeck(@RequestBody Boolean speak){
        this.debateGui.canSpeak(speak);
    }
}
