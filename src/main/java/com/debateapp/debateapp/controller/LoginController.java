package com.debateapp.debateapp.controller;

import com.debateapp.debateapp.config.Publisher;
import com.debateapp.debateapp.config.Subscriber;
import com.debateapp.debateapp.dto.Person;
import com.debateapp.debateapp.dto.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("${endpoint.login}")
public class LoginController {

    public static List<Person> users;
    public static String userLogged = "";

    private TokenController tokenController;
    private Environment environment;
    private String ip;

    @Autowired
    public LoginController(Environment environment, TokenController tokenController) throws UnknownHostException {
        this.environment = environment;
        this.tokenController = tokenController;
        this.ip = InetAddress.getLocalHost().getHostAddress();

        LoginController.users = new ArrayList<>();
        LoginController.users.add(new Person("geo", List.of(Topic.Education, Topic.Art), List.of(Topic.Art)));
        LoginController.users.add(new Person("maia", List.of(Topic.Education), List.of(Topic.Finance)));
        LoginController.users.add(new Person("tudor", List.of(Topic.Education, Topic.Finance), List.of(Topic.Education)));
        LoginController.users.add(new Person("rares", List.of(Topic.Art), List.of(Topic.Art, Topic.Finance)));
    }

    @PostMapping
    public boolean login(String name){
        for(Person p : users){
            if (p.getName().toLowerCase().equals(name.toLowerCase()))
            {
                LoginController.userLogged = name;
                setPubSubTopics(p);
                tokenController.joinTopology(p);

                String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.debate") + "/opendebates";
                new RestTemplate().postForObject(url, p, Object.class);
                return true;
            }
        }
        return false;
    }

    private void setPubSubTopics(Person person){
        person.getTopicPublisher().forEach(topic -> {
            if(topic.equals(Topic.Education)){
                Publisher.publisherEducation = true;
            }
            else if(topic.equals(Topic.Finance)){
                Publisher.publisherFinance = true;
            }
            else{
                Publisher.publisherArt = true;
            }
        });

        person.getTopicSubscriber().forEach(topic -> {
            if(topic.equals(Topic.Education)){
                Subscriber.subscriberEducation = true;
            }
            else if(topic.equals(Topic.Finance)){
                Subscriber.subscriberFinance = true;
            }
            else{
                Subscriber.subscriberArt = true;
            }
        });
    }

}
