package com.debateapp.debateapp.controller;

import com.debateapp.debateapp.config.Publisher;
import com.debateapp.debateapp.config.Subscriber;
import com.debateapp.debateapp.dto.Message;
import com.debateapp.debateapp.dto.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${endpoint.pubsub}")
public class PubSubController {

    Logger logger = LoggerFactory.getLogger(PubSubController.class);
    private Publisher publisher;
    private Subscriber subscriber;

    @Autowired
    public PubSubController(Publisher publisher, Subscriber subscriber) {
        this.publisher = publisher;
        this.subscriber = subscriber;
    }

    @PostMapping
    public void publishEducation(@RequestBody Message message){
        publisher.publish(message.getTopic(), message);
    }

    @GetMapping(value = "/{topic}")
    public List<Message> getMessage(@PathVariable("topic") Topic topic){
        switch (topic){
            case Art:
                return subscriber.getArtMessages();
            case Education:
                return subscriber.getEducationMessages();
            case Finance:
                return subscriber.getFinanceMessages();
            default:
                return null;
        }
    }
}
