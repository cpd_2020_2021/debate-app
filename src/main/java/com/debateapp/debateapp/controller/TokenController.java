package com.debateapp.debateapp.controller;

import com.debateapp.debateapp.dto.Person;
import com.debateapp.debateapp.socket.SocketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class TokenController {

    public static int nodId = 0;
    public static int timeToSpeak;
    private static boolean canSpeak;

    private static Environment environment;
    private static String ip;

    private SocketManager serverSocket;

    @Autowired
    public TokenController(Environment environment) throws UnknownHostException {
        TokenController.environment = environment;
        TokenController.ip = InetAddress.getLocalHost().getHostAddress();
        TokenController.timeToSpeak = 20000; //can speak for 20 sec

        // 0 is a reserved id
        while(TokenController.nodId == 0){
            TokenController.nodId = ThreadLocalRandom.current().nextInt();
        }
        canSpeak = false;
    }

    public void joinTopology(Person person){
        String serverGetter = "socket." + person.getName().toLowerCase() + ".server";
        String rightGetter = "socket." + person.getName().toLowerCase() + ".right";

        int serverConnection = Integer.parseInt(Objects.requireNonNull(this.environment.getProperty(serverGetter)));
        int rightConnection = Integer.parseInt(Objects.requireNonNull(this.environment.getProperty(rightGetter)));

        this.serverSocket = new SocketManager();
        this.serverSocket.startSocketCommunication(serverConnection, rightConnection, ip);
    }

    public static boolean isCanSpeak() {
        return canSpeak;
    }

    public static void setCanSpeak(boolean canSpeak) {
        TokenController.canSpeak = canSpeak;
        String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.debate") + "/tokenreceived";
        new RestTemplate().postForObject(url, canSpeak, Object.class);
    }
}
