package com.debateapp.debateapp.gui;

import com.debateapp.debateapp.controller.LoginController;
import com.debateapp.debateapp.controller.TokenController;
import com.debateapp.debateapp.dto.Message;
import com.debateapp.debateapp.dto.Person;
import com.debateapp.debateapp.dto.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@EnableAsync
@EnableScheduling
public class DebateGui {
    private JFrame jFrame;
    private JPanel rootPanel;
    private JLabel exit;
    private JLabel minimize;
    private JPanel titleL;
    private JLabel clock;
    private JPanel debatePanel;
    private JPanel timerPanel;
    private JPanel topicsPanel;
    private JPanel chatPanel;
    private JButton educationButton;
    private JButton financeButton;
    private JButton artButton;
    private JPanel sendPanel;
    private JTextField writeMessage;
    private JButton sendButton;
    private JScrollPane messagesJScrollPanel;
    private JLabel activeTopicLabel;
    private JLabel usernameText;
    private JTextPane chatPane;

    private int time;
    private Environment environment;
    private String ip;
    private Topic activeTopic;
    private Map<Topic, Boolean> publishers;
    private Map<Topic, Boolean> subscriptions;


    @Autowired
    public DebateGui(Environment environment) throws UnknownHostException {

        this.environment = environment;
        ip = InetAddress.getLocalHost().getHostAddress();

        jFrame = new JFrame();
        jFrame.add(rootPanel);

        exit.setCursor( new Cursor(Cursor.HAND_CURSOR) );
        exit.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });

        minimize.setCursor( new Cursor (Cursor.HAND_CURSOR) );
        minimize.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jFrame.setState(JFrame.ICONIFIED);
            }
        });

        educationButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setActiveTopic(Topic.Education);
            }
        });

        financeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setActiveTopic(Topic.Finance);
            }
        });

        artButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setActiveTopic(Topic.Art);
            }
        });

        sendButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(!publishers.get(activeTopic)) return;
                if(time < 0) return; //the person can t speakgeo

                String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.debate");
                Message message = new Message(usernameText.getText(), writeMessage.getText(), activeTopic);
                new RestTemplate().postForObject(url, message, Object.class);
                writeMessage.setText("Type your message hear...");
                displayMessage(message);
            }
        });

        this.jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.jFrame.setUndecorated(true);
        this.jFrame.setSize(800, 650);
        this.jFrame.setLocationRelativeTo(null);
    }

    public void setVisibility(boolean visibility, Person person){
        this.jFrame.setVisible(visibility);
        initGui(person);
        Optional<Topic> topic = Stream.of(Topic.values()).filter(t -> this.subscriptions.get(t) || this.publishers.get(t)).findFirst();
        if (topic.isPresent()) {
            setActiveTopic(topic.get());
        }
    }

    public void canSpeak(boolean speak){
        if (speak)
            time = TokenController.timeToSpeak/1000;
        else
            time = -1;
    }

    public void displayMessage(Message message){
        if (LoginController.userLogged.toLowerCase().equals(message.getPublisher().toLowerCase())){
            appendToPane(chatPane, message.forGui(), Color.red);
        }
        else{
            if (!this.subscriptions.get(activeTopic)) return;
            appendToPane(chatPane, message.forGui(), Color.ORANGE);
        }
    }

    private void setActiveTopic(Topic topic){

        educationButton.setBackground(new Color(208, 208, 208));
        educationButton.setForeground(new Color(35, 31, 31));
        financeButton.setBackground(new Color(208, 208, 208));
        financeButton.setForeground(new Color(35, 31, 31));
        artButton.setBackground(new Color(208, 208, 208));
        artButton.setForeground(new Color(35, 31, 31));

        activeTopic = topic;
        activeTopicLabel.setText(activeTopic.toString());

        switch (activeTopic){
            case Education:
                educationButton.setBackground(new Color(65, 15, 20));
                educationButton.setForeground(new Color(250, 249, 242));
                break;
            case Finance:
                financeButton.setBackground(new Color(65, 15, 20));
                financeButton.setForeground(new Color(250, 249, 242));
                break;
            case Art:
                artButton.setBackground(new Color(65, 15, 20));
                artButton.setForeground(new Color(250, 249, 242));
                break;
        }

        if(this.publishers.get(activeTopic)) {
            writeMessage.setEditable(true);
            writeMessage.setText("Type your message hear...");
        }else{
            writeMessage.setEditable(false);
            writeMessage.setText("You are not a publisher on this topic!");
        }

        chatPane.setText("");
        if(this.subscriptions.get(activeTopic)) {
            String url = "http://" + ip  + ":" + environment.getProperty("local.server.port")  +  environment.getProperty("endpoint.pubsub") + "/{query}";
            ResponseEntity<Message[]> response = new RestTemplate().getForEntity(url, Message[].class, activeTopic);
            Message[] messages = response.getBody();
            for(Message message : messages)
                displayMessage(message);
        }else{
            appendToPane(chatPane, "You are not a subscriber on this topic!\n\n", Color.red);
        }
    }

    private void appendToPane(JTextPane tp, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }

    private void initGui(Person person){
        this.usernameText.setText(LoginController.userLogged.substring(0,1).toUpperCase() + LoginController.userLogged.substring(1).toLowerCase());

        this.publishers = new HashMap<>();
        this.subscriptions = new HashMap<>();
        Stream.of(Topic.values()).forEach(t -> {
            this.publishers.put(t, false);
            this.subscriptions.put(t, false);
        });

        person.getTopicPublisher().forEach(topic -> this.publishers.put(topic, true));
        person.getTopicSubscriber().forEach(topic -> this.subscriptions.put(topic, true));

        if (!this.subscriptions.get(Topic.Education) && !this.publishers.get(Topic.Education)) {
            this.topicsPanel.remove(educationButton);
        }
        if (!this.subscriptions.get(Topic.Finance) && !this.publishers.get(Topic.Finance)) {
            this.topicsPanel.remove(financeButton);
        }
        if (!this.subscriptions.get(Topic.Art) && !this.publishers.get(Topic.Art)) {
            this.topicsPanel.remove(artButton);
        }
    }

    @Scheduled(fixedDelay=1000)
    private void updateClock(){

        if( TokenController.isCanSpeak() ){
            clock.setText(time + "");
            if (time > 0) time--;
        }else{
            clock.setText("You don`t have the token.");
        }
    }

}
