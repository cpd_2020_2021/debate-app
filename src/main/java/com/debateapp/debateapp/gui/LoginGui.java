package com.debateapp.debateapp.gui;

import com.debateapp.debateapp.controller.LoginController;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@Component
public class LoginGui {

    private JFrame jFrame;
    private JLabel exit;
    private JLabel minimize;
    private JPanel titleL;
    private JTextField usernameLogin;
    private JLabel usernameLabel;
    private JPanel btnPanel;
    private JButton loginBtn;
    private JPanel rootPanel;

    private LoginController loginController;

    @Autowired
    public LoginGui(LoginController loginController) {

        this.loginController = loginController;

        jFrame = new JFrame();
        jFrame.add(rootPanel);

        exit.setCursor( new Cursor(Cursor.HAND_CURSOR) );
        exit.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });

        minimize.setCursor( new Cursor (Cursor.HAND_CURSOR) );
        minimize.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                jFrame.setState(JFrame.ICONIFIED);
            }
        });

        loginBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean status = loginController.login(usernameLogin.getText());
                if(status){
                    setVisibility(false);
                }
            }
        });

        this.jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.jFrame.setUndecorated(true);
        this.jFrame.setSize(400, 300);
        this.jFrame.setLocationRelativeTo(null);
        this.jFrame.setVisible(true);
    }

    public void setVisibility(boolean visibility){
        this.jFrame.setVisible(visibility);
    }
}
