package com.debateapp.debateapp.dto;

public enum Topic {
    Education,
    Finance,
    Art
}
