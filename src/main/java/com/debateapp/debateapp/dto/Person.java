package com.debateapp.debateapp.dto;

import java.util.List;

public class Person {

    String name;
    List<Topic> topicPublisher;
    List<Topic> topicSubscriber;

    public Person(String name, List<Topic> topicPublisher, List<Topic> topicSubscriber) {
        this.name = name;
        this.topicPublisher = topicPublisher;
        this.topicSubscriber = topicSubscriber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Topic> getTopicPublisher() {
        return topicPublisher;
    }

    public void setTopicPublisher(List<Topic> topicPublisher) {
        this.topicPublisher = topicPublisher;
    }

    public List<Topic> getTopicSubscriber() {
        return topicSubscriber;
    }

    public void setTopicSubscriber(List<Topic> topicSubscriber) {
        this.topicSubscriber = topicSubscriber;
    }
}
