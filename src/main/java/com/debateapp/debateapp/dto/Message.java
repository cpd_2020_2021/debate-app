package com.debateapp.debateapp.dto;

public class Message {

    private String publisher;
    private String message;
    private Topic topic;

    public Message() {
    }

    public Message(String publisher, String message, Topic topic) {
        this.publisher = publisher;
        this.message = message;
        this.topic = topic;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "Message{" +
                "publisher='" + publisher + '\'' +
                ", message='" + message + '\'' +
                ", topic=" + topic +
                '}';
    }

    public String forGui(){
        return publisher + ": " +  message + "\n\n";
    }
}
