package com.debateapp.debateapp.socket;

import com.debateapp.debateapp.controller.LoginController;
import com.debateapp.debateapp.controller.TokenController;
import lombok.SneakyThrows;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ThreadLocalRandom;

public class SocketManager extends Thread{

    private int serverPort;
    private PrintWriter rightOut;
    private String message;

    private volatile boolean connectedToRightNeighbor;
    private boolean masterChosen;
    private boolean master;


    public void startSocketCommunication(int serverPort, int rightPort, String ip) {
        this.serverPort = serverPort;
        this.start();

        this.connectedToRightNeighbor = false;
        this.masterChosen = false;
        this.master = false;

        while (!connectedToRightNeighbor) {
            try {
                Socket rightSocket = new Socket(ip, rightPort);
                rightOut = new PrintWriter(rightSocket.getOutputStream(), true);
                connectedToRightNeighbor = true;
            } catch (IOException ignored) {
            }
        }

        // Geo will start the process to chose the master my sending its node id to the right neighbor
        if(LoginController.userLogged.toLowerCase().equals("geo")){
            rightOut.println(TokenController.nodId);
        }
    }

    @SneakyThrows
    @Override
    public void run() {
        java.net.ServerSocket serverSocket = new java.net.ServerSocket(this.serverPort);
        Socket clientSocket = serverSocket.accept();
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        while (true) {
            this.message = in.readLine();
            if (this.message != null) {
                while (!connectedToRightNeighbor) Thread.onSpinWait();
                if (!masterChosen) {
                    // Master will by the user with the biggest node it
                    // Step 1:  When a node receives a number equal with its id => he is the master
                    // Step 2: After a node knows he is the master notifies the rest of the users with a 0 message
                    // Step 3: The others receives the 0 message <=> notified that the master was chosen
                    // Step 4: Master receives the 0 message <=> the rest of the users know about the master and choose a token to send
                    int msg = Integer.parseInt(message);
                    if (msg == 0) {
                        masterChosen = true;
                        if (master){
                            System.out.println("[+] " + LoginController.userLogged + ": The debate starts now with master");
                            int token = ThreadLocalRandom.current().nextInt();
                            TokenController.setCanSpeak(true);
                            Thread.sleep(TokenController.timeToSpeak);
                            TokenController.setCanSpeak(false);
                            rightOut.println(token);
                        }else{
                            rightOut.println(message);
                        }
                    } else if (msg == TokenController.nodId) {
                        master = true;
                        rightOut.println(0);
                    } else if (msg > TokenController.nodId) {
                        rightOut.println(message);
                    } else {
                        rightOut.println(TokenController.nodId);
                    }
                } else {
                    TokenController.setCanSpeak(true);
                    new java.util.Timer().schedule(new java.util.TimerTask() {
                                                       @Override
                                                       public void run() {
                                                           rightOut.println(message);
                                                           TokenController.setCanSpeak(false);
                                                           System.out.println("Bye token!");
                                                       }
                                                   },
                            TokenController.timeToSpeak
                    );
                    System.out.println(LoginController.userLogged + ": " + this.message);
                }
            }
        }
    }
}
